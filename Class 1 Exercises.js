/**
 * MATH
 */

// 1. Pagliacci charges $16.99 for a 13” pizza and $19.99 for a 17” pizza.
// What is the area for each of these pizzas?
// (radius would be the listed size - i.e. 13" - divided by 2)

const num13 = 13
const num17 = 17
const numcost13 = 16.99
const numcost17 = 19.99

let area1 = Math.PI * (num13 / 2) ** 2
let area2 = Math.PI * (num17 / 2) ** 2

area1
//132.73228961416876

area2
//226.98006922186255



// 2. What is the cost per square inch of each pizza?

const num13 = 13
const num17 = 17
const numcost13 = 16.99
const numcost17 = 19.99

let area1 = Math.PI * (num13 / 2) ** 2
let area2 = Math.PI * (num17 / 2) ** 2

let cost1 = numcost13 / area1
let cost2 = numcost17 / area2

cost1
//0.1280020110358125

cost2
//0.08806940657181973



// 3. Using the Math object, put together a code snippet
// that allows you to draw a random card with a value
// between 1 and 13 (assume ace is 1, jack is 11…)

let cardone = Math.round(Math.random() * 13 + 1)
let cardtwo = Math.round(Math.random() * 13 + 1)
let cardthree = Math.round(Math.random() * 13 + 1)

cardone
//10

cardtwo
//11

cardthree
//6



// 4. Draw 3 cards and use Math to determine the highest
// card

let cardone = Math.round(Math.random() * 13 + 1)
let cardtwo = Math.round(Math.random() * 13 + 1)
let cardthree = Math.round(Math.random() * 13 + 1)

cardone
//10

cardtwo
//11

cardthree
//6

let highCard = Math.max(cardone, cardtwo, cardthree)

highCard
//11



/**
 * ADDRESS LINE
 */

// 1. Create variables for firstName, lastName,
// streetAddress, city, state, and zipCode. Use
// this information to create a formatted address block
// that could be printed onto an envelope.

const firstName = "Brian"
const lastName = "Cabacungan"
const streetAddress = "Street St"
const city = "San Diego"
const state = "CA"
const zipCode = "92115"

firstName + " " + lastName + "\n" + streetAddress + "\n" + city + ", " + state + " " + zipCode
/** "Brian Cabacungan
 *   Street St
 *   San Diego, CA 92115"
 */



// 2. You are given a string in this format:
// firstName lastName(assume no spaces in either)
// streetAddress
// city, state zip(could be spaces in city and state)
// 
// Write code that is able to extract the first name from this string into a variable.
// Hint: use indexOf, slice, and / or substring

const firstName = "Brian"
const lastName = "Cabacungan"
const streetAddress = "Street St"
const city = "San Diego"
const state = "CA"
const zipCode = "92115"

firstName + " " + lastName + "\n" + streetAddress + "\n" + city + ", " + state + " " + zipCode
/** "Brian Cabacungan
 *   Street St
 *   San Diego, CA 92115"
 */
 
let addressStr = firstName + " " + lastName + "\n" + streetAddress + "\n" + city + ", " + state + " " + zipCode
let firstNameIndex = addressStr.substring(0,5)

firstNameIndex
//"Brian"



/**
 * FIND THE MIDDLE DATE
 */
// On your own find the middle date(and time) between the following two dates:
// 1/1/2020 00:00:00 and 4/1/2020 00:00:00
//
// Look online for documentation on Date objects.

// Starting hint:
const endDate = new Date(2019, 3, 1);

new Date()
//Tue Jan 19 2021 20:34:58 GMT-0800 (Pacific Standard Time)

const date1 = new Date(2020, 0, 1, 0, 0)
const date2 = new Date(2020, 3, 1, 0, 0)
const midDate = new Date((date1.getTime() + date2.getTime()) / 2)

date1
//Wed Jan 01 2020 00:00:00 GMT-0800 (Pacific Standard Time)

date2
//Wed Apr 01 2020 00:00:00 GMT-0700 (Pacific Daylight Time)

midDate
//Sat Feb 15 2020 11:30:00 GMT-0800 (Pacific Standard Time)
